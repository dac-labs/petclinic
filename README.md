# Petclinic

This is a continuation of the DevOps and Cloud Labs Project. This project is responsible for producing the container and packing the petclinic app.

Petclinic Repo: https://github.com/spring-projects/spring-petclinic

The project was project challenge from a DevOps community named DevOps and Cloud Labs, anyone can join us at https://www.youtube.com/@DevOps-Cloud. We are exited and we will wait for you there.

## Objectives

#### 1. Sonarqube Repo Scan

The pipeline must scan the code using Sonarqube.

#### 2. Publish .jar package to GitLab pipeline package registry

The pipeline must generate the .jar binary file for the petclinic and publish it to the project package registry.

#### 3. Containerize the application to the Gitlab pipeline container registry

The pipeline must containerize the application and publish it to the probject container registry.

## Pipeline Job Documentation

#### 1. Make Settings Files

Job "make settings files" is responsible for creating 2 xml files namely, pom.xml and settings.xml.

pom.xml is a required file that is required by the petclinic file. The pipeline job will run the "envsubst" program and substitute the environment variable specified in the repository section and replace them with the actual value of that variable and save a copy of it (pom.xml) to the root directory for the current job.

In this case it is the $GROUP_ID and $PROJECT_ID variables that are being replaced.

```
<repositories>
    <repository>
      <id>gitlab-maven</id>
      <url>https://gitlab.com/api/v4/groups/$GROUP_ID/-/packages/maven</url>
    </repository>
  </repositories>
  <distributionManagement>
    <repository>
      <id>gitlab-maven</id>
      <url>https://gitlab.com/api/v4/projects/$PROJECT_ID/packages/maven</url>
    </repository>
    <snapshotRepository>
      <id>gitlab-maven</id>
      <url>https://gitlab.com/api/v4/projects/$PROJECT_ID/packages/maven</url>
    </snapshotRepository>
  </distributionManagement>
```

settings.xml is a required file that maven uses to publish the .jar file to the pipeline package registry.

#### 2. Scanning petclinic code hygine

Job "sonarcloud-check" is responsible for performing the code quality for potential vulnerabilities.

#### 3. Building the Petclinic binary file

Job "build artifact" creates the binary file (.jar) that is used by the following jobs.

#### 4. Publish the package to the pipeline package registry

Job "push package to registry" publish the binary file (.jar) to the pipeline package registry for later use.

#### 5. Containerize the application and Publish to pipeline container registry

Job "push container to regsitry" uses Dockerfile to create a local container image for the petclinic and assign tags to it. It then publish the tagged container image to the Pipeline container registry.

## Acknowledgements

- Thank you to the Admins of the group at [DevOps and Cloud Labs](https://awesomeopensource.com/project/elangosundar/awesome-README-templates) Channel on giving us beginner friendly projects that allows us to push the limits on what we can do despite lacking expience.
